
package main;

import javafx.geometry.Pos;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author sherzod2013
 */
public class AboutPane extends VBox
{
    private static final String PAGE_LINK = "https://gitlab.com/sherzod2013exe/kurs-ishi";

    private Stage primaryStage;

    public AboutPane(Stage primaryStage)
    {
        super();
        this.primaryStage = primaryStage;
        createWindow();

    }

    private void createWindow()
    {
        HBox contactHBox = new HBox();
        contactHBox.setAlignment(Pos.CENTER);
        Hyperlink website = new Hyperlink(PAGE_LINK);
        website.setOnAction(event -> new HtmlPage(PAGE_LINK).show());
        contactHBox.getChildren().addAll(new Label("Please visit me at"), website);
        getChildren().addAll(new Label("Version 1.0"), contactHBox);
        setAlignment(Pos.CENTER);
        setSpacing(0);
    }

}
